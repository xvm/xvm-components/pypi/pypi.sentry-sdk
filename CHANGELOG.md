# Changelog

## v1.45.0

* update sentry-python to v1.45.0

## v1.10.1

* update sentry-python to v1.10.1

## v1.9.5

* first release
